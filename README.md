# My Menu
**Restaurant menu in a spreadsheet database.**

This is a GitLab pages project which implements the Google Sheets API as a database for a restaurant web page.

* Find the Google Sheets API documentation [in here.](https://developers.google.com/sheets/api/guides/values)
* Check the project's [demo.](https://tavoohoh.gitlab.io/mymenu/) and official site [here.](https://elfarolitove.com/)
* And [here](https://docs.google.com/spreadsheets/d/1nC3UmfdwucnfZOovs_5vJs7qZ5hvvlnza3ySieXBR5E/edit#gid=0) the sheet database.