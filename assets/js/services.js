var database = '1nC3UmfdwucnfZOovs_5vJs7qZ5hvvlnza3ySieXBR5E';
var whatsapp_number;
var date;

$(document).ready(function () {
  $('body').addClass('loader');

  getDate();
});

function onReadyContent() {
  $('body').removeClass('loader');
  $('header').show(0);

  setTimeout(() => {
    $('.content').show(0);
  }, 500);

  setTimeout(() => {
    $('footer').show(0);
  }, 600);
}

/**
 * Getting tables data
 */
function getTablesData() {
  metadataTable();
  mealsTable();
  sidedTable();
  soupTable();
}

function metadataTable() {
  var params = {
    spreadsheetId: database,
    range: 'metadata',
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function (response) {
    insertMetadataTable(response.result.values);
  }, function (reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function mealsTable() {
  var params = {
    spreadsheetId: database,
    range: 'platos',
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function (response) {
    insertMealsTable(response.result.values, "meals");
  }, function (reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function sidedTable() {
  var params = {
    spreadsheetId: database,
    range: 'contornos',
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function (response) {
    insertSidedTable(response.result.values);
  }, function (reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function soupTable() {
  var params = {
    spreadsheetId: database,
    range: 'sopas',
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function (response) {
    insertSoupTable(response.result.values);
  }, function (reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function drinkTable() {
  var params = {
    spreadsheetId: database,
    range: 'bebidas',
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function (response) {
    insertDrinkTable(response.result.values, "drink");
  }, function (reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function desseTable() {
  var params = {
    spreadsheetId: database,
    range: 'postres',
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function (response) {
    insertDesseTable(response.result.values, "desse");
  }, function (reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

/**
 * Render data
 */
function insertMetadataTable(data) {
  $('#meta_logo').attr('src', data[0][1]);
  $('#meta_name')[0].textContent = data[1][1];
  $('#meta_type')[0].textContent = data[2][1];
  $('#meta_desc')[0].textContent = data[3][1];
  $('#meta_addr')[0].textContent = data[4][1];
  $('#meta_telf')[0].textContent = data[5][1];
  $('#meta_telf').attr('href', 'tel:+' + data[5][1]);
  $('#meta_fb').attr('href', data[6][1]);
  $('#meta_tw').attr('href', data[7][1]);
  $('#meta_ig').attr('href', data[8][1]);
  whatsapp_number = data[9][1];
};

function insertMealsTable(data, id) {

  for (let n = 1; n < data.length; n++) {

    if (data[n][0] === 'x') {

      /* recipe detail */
      var card = document.createElement('span');
      var imge = document.createElement('div');
      var info = document.createElement('div');
      var info_grid = document.createElement('div');
      var titl = document.createElement('b');
      var desc = document.createElement('p');
      var pric = document.createElement('small');
      var extp = document.createElement('p');
      var extp_ul = document.createElement('ul');
      var butt = document.createElement('button');

      var titl_content = document.createTextNode(data[n][1]);
      var desc_content = document.createTextNode(data[n][2]);
      var pric_content = document.createTextNode(data[n][3] + ' Bs.');
      var butt_content = document.createTextNode('ordenar');
      var extp_p_content = document.createTextNode(data[0][6] + ':');

      info_grid.className = 'card_info_grid';

      if (data[1][7] === 'x') {
        console.log('with img');
        card.className = 'card';
        imge.className = 'card_imag';
        info.className = 'card_info';
      } else {
        console.log('no img');
        card.className = 'card_no_img';
        imge.className = 'card_imag_no_img';
        info.className = 'card_info_no_img';
      }

      imge.style.backgroundImage = "url('" + data[n][4] + "')";

      titl.appendChild(titl_content);
      desc.appendChild(desc_content);
      pric.appendChild(pric_content);
      extp.appendChild(extp_p_content);
      butt.appendChild(butt_content);

      butt.onclick = function () {
        setOrder(data[n][1]);
      };

      for (let n = 1; n < data.length; n++) {
        if (data[n][6]) {
          var extp_li = document.createElement('li');
          var extp_li_content = document.createTextNode(data[n][6]);
          extp_li.appendChild(extp_li_content);
          extp_ul.appendChild(extp_li);
        }
      }

      /* append to info grid */
      // info_grid.appendChild(extp);
      info_grid.appendChild(extp_ul);
      info_grid.appendChild(pric);
      info_grid.appendChild(butt);

      /* append to info */
      info.appendChild(titl);
      // info.appendChild(desc);
      info.appendChild(info_grid);

      /* append to parent node */
      card.appendChild(imge);
      card.appendChild(info);

      document.getElementById(id).appendChild(card);

      /* display content */
      $('#loading').hide(0, null, onReadyContent());
    }
  };
};

function insertSidedTable(data) {

  /* First sidedish */
  var sidedish_1 = document.getElementById('sidedish_1');
  var sidedish_2 = document.getElementById('sidedish_2');

  /* dish options */
  for (var i = 1; i < data.length; i++) {
    var option_1 = document.createElement('option');
    option_1.value = data[i][0];
    option_1.text = data[i][0];
    sidedish_1.add(option_1);

    var option_2 = document.createElement('option');
    option_2.value = data[i][0];
    option_2.text = data[i][0];
    sidedish_2.add(option_2);
  }

};

function insertSoupTable(data) {

  /* soup select */
  var soup = document.getElementById('soup');

  for (var i = 1; i < data.length; i++) {
    var option = document.createElement('option');
    option.value = data[i][0];
    option.text = data[i][0];
    soup.add(option);
  }
}

function insertDrinkTable(data, id) {

  $('#drink_tit')[0].textContent = 'Bebidas';

  for (let n = 1; n < data.length; n++) {
    var card = document.createElement('span');

    var titl = document.createElement('b');
    var desc = document.createElement('p');

    var titl_content = document.createTextNode(data[n][0]);
    var desc_content = document.createTextNode(data[n][1]);

    titl.appendChild(titl_content);
    desc.appendChild(desc_content);

    card.appendChild(titl);

    if (data[n][1]) {
      card.appendChild(desc);
    }

    document.getElementById(id).appendChild(card);
  };
};

function insertDesseTable(data, id) {

  $('#desse_tit')[0].textContent = 'Postres';

  for (let n = 1; n < data.length; n++) {
    var card = document.createElement('span');

    var titl = document.createElement('b');
    var desc = document.createElement('p');

    var titl_content = document.createTextNode(data[n][0]);
    var desc_content = document.createTextNode(data[n][1]);

    titl.appendChild(titl_content);
    desc.appendChild(desc_content);

    card.appendChild(titl);

    if (data[n][1]) {
      card.appendChild(desc);
    }

    document.getElementById(id).appendChild(card);
  };
};

/*
    Getting date
*/
function getDate() {
  var objToday = new Date(),
    weekday = new Array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'),
    dayOfWeek = weekday[objToday.getDay()],
    domEnder = function () { var a = objToday; if (/1/.test(parseInt((a + "").charAt(0)))) return "th"; a = parseInt((a + "").charAt(1)); return 1 == a ? "st" : 2 == a ? "nd" : 3 == a ? "rd" : "th" }(),
    dayOfMonth = today + (objToday.getDate() < 10) ? '0' + objToday.getDate() : objToday.getDate(),
    months = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'),
    curMonth = months[objToday.getMonth()],
    curYear = objToday.getFullYear(),
    curHour = objToday.getHours() > 12 ? objToday.getHours() - 12 : (objToday.getHours() < 10 ? "0" + objToday.getHours() : objToday.getHours()),
    curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes(),
    curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds(),
    curMeridiem = objToday.getHours() > 12 ? "PM" : "AM";

  // var today = curHour + ":" + curMinute + "." + curSeconds + curMeridiem + " " + dayOfWeek + " " + dayOfMonth + " of " + curMonth + ", " + curYear;
  var today = dayOfWeek + ', ' + dayOfMonth + " de " + curMonth + " de " + curYear;
  date = dayOfWeek + ', ' + dayOfMonth + " de " + curMonth + " de " + curYear + " | " + curHour + ":" + curMinute + " " + curMeridiem;

  $('#date')[0].textContent = today;
}

/*
    Google API Inits
*/

function initClient() {

  var API_KEY = 'AIzaSyAtzMVIDGJa82o6TKE43shjWqNYaSAN5sk'
  var CLIENT_ID = '540414195371-75pgorgl95imaqqb4l3717spihtdk0ac.apps.googleusercontent.com';
  var SCOPE = 'https://www.googleapis.com/auth/drive.readonly';

  gapi.client.init({
    'apiKey': API_KEY,
    'clientId': CLIENT_ID,
    'scope': SCOPE,
    'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
  }).then(function () {
    getTablesData();
  });
}

function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/*
    Sent whatsapp message
*/
function setOrder(order) {
  $('#setOrder').show();
  $('#setOrder h2')[0].textContent = order;
}

function closeModal() {
  $('#setOrder').hide();
}

function sendMenssage() {
  var meal = $('#setOrder h2').text();
  // writeBuyButton(meal);

  /* get sidedish */
  var side_1 = 'Sin primer contorno';
  var side_2 = 'Sin segundo contorno';
  if ($('#sidedish_1').val()) {
    side_1 = $('#sidedish_1').val();
  }
  if ($('#sidedish_2').val()) {
    side_2 = $('#sidedish_2').val();
  }

  /* get soup */
  var soup = 'Sin sopa';
  if ($('#soup').val()) {
    soup = $('#soup').val();
  }

  /* get deliver */
  var deliver = 'Entrega delivery';

  if (document.getElementById('local').checked) {
    deliver = 'Para comer en el restaurant';
  } else if (document.getElementById('viaje').checked) {
    deliver = 'Para llevar';
  } else {
    deliver = 'Entrega delivery';
  }

  /* set order text */
  var mealdish = '*Plato:* ' + meal + '.\n';
  var sidedish = '*Contornos:* ' + side_1 + ', ' + side_2 + '.\n';
  var soupdish = '*Sopa*: ' + soup + '.\n';
  var delivery = '*' + deliver + '*';

  var phone = whatsapp_number;
  var text = 'Hola, me gustaría realizar la siguiente orden:\n' + mealdish + sidedish + soupdish + delivery;

  var api_link = 'https://web.whatsapp.com/send?|=es&phone=<number>&text=<text>';

  if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
    api_link = 'https://wa.me/<number>?text=<text>';
  }

  api_link = api_link.replace('<number>', phone);
  api_link = api_link.replace('<text>', text);
  api_link = encodeURI(api_link);

  closeModal();
  window.open(api_link, '_blank');
}
